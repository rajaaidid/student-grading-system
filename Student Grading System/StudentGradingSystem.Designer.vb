﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Student_Grading_System
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Student_Grading_System))
        Me.sidebarSplitter = New System.Windows.Forms.Splitter()
        Me.btnManageSubjects = New System.Windows.Forms.Button()
        Me.btnManageLecturers = New System.Windows.Forms.Button()
        Me.mainIcon = New System.Windows.Forms.Label()
        Me.btnManageClasses = New System.Windows.Forms.Button()
        Me.btnManageStudentsGPA = New System.Windows.Forms.Button()
        Me.subjectPanel = New System.Windows.Forms.Panel()
        Me.subjectDataGrid = New System.Windows.Forms.DataGridView()
        Me.subject_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subject_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subject_description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEditCourse = New System.Windows.Forms.Button()
        Me.btnAddCourse = New System.Windows.Forms.Button()
        Me.btnDeleteCourse = New System.Windows.Forms.Button()
        Me.btnUpdateCourse = New System.Windows.Forms.Button()
        Me.btnLoadCourse = New System.Windows.Forms.Button()
        Me.btnClearAllCourse = New System.Windows.Forms.Button()
        Me.actionSplitter = New System.Windows.Forms.Splitter()
        Me.txtSubjectDescription = New System.Windows.Forms.TextBox()
        Me.lblSubjectDescription = New System.Windows.Forms.Label()
        Me.lblSubjectName = New System.Windows.Forms.Label()
        Me.txtSubjectName = New System.Windows.Forms.TextBox()
        Me.lblSubjectID = New System.Windows.Forms.Label()
        Me.txtSubjectID = New System.Windows.Forms.TextBox()
        Me.CcourseBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Student_Grading_System_DataSet1 = New StudentGradingSystem.Student_Grading_System_DataSet()
        Me.Student_Grading_System_DataSet = New StudentGradingSystem.Student_Grading_System_DataSet()
        Me.C_courseTableAdapter = New StudentGradingSystem.Student_Grading_System_DataSetTableAdapters.c_courseTableAdapter()
        Me.CcourseBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lecturerPanel = New System.Windows.Forms.Panel()
        Me.txtLecturerEmail = New System.Windows.Forms.TextBox()
        Me.lblLecturerEmail = New System.Windows.Forms.Label()
        Me.txtLecturerContact = New System.Windows.Forms.TextBox()
        Me.lecturerDataGrid = New System.Windows.Forms.DataGridView()
        Me.lecturer_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lecturer_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lecturer_contact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lecturer_email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnAddLecturer = New System.Windows.Forms.Button()
        Me.btnDeleteLecturer = New System.Windows.Forms.Button()
        Me.btnUpdateLecturer = New System.Windows.Forms.Button()
        Me.btnLoadLecturer = New System.Windows.Forms.Button()
        Me.btnClearAllLecturer = New System.Windows.Forms.Button()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.lblLecturerContact = New System.Windows.Forms.Label()
        Me.lblLecturerName = New System.Windows.Forms.Label()
        Me.txtLecturerName = New System.Windows.Forms.TextBox()
        Me.lblLecturerID = New System.Windows.Forms.Label()
        Me.txtLecturerID = New System.Windows.Forms.TextBox()
        Me.classesPanel = New System.Windows.Forms.Panel()
        Me.cboLecturer = New System.Windows.Forms.ComboBox()
        Me.cboSubjectName = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCreditHour = New System.Windows.Forms.TextBox()
        Me.classDataGrid = New System.Windows.Forms.DataGridView()
        Me.class_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subject_name_classes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.credit_hour = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lecturer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSaveClass = New System.Windows.Forms.Button()
        Me.btnAddClass = New System.Windows.Forms.Button()
        Me.btnDeleteClass = New System.Windows.Forms.Button()
        Me.btnUpdateClass = New System.Windows.Forms.Button()
        Me.btnLoadClass = New System.Windows.Forms.Button()
        Me.btnClearAllClass = New System.Windows.Forms.Button()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtClassID = New System.Windows.Forms.TextBox()
        Me.btnManageStudentsCGPA = New System.Windows.Forms.Button()
        Me.subjectPanel.SuspendLayout()
        CType(Me.subjectDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CcourseBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Student_Grading_System_DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Student_Grading_System_DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CcourseBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lecturerPanel.SuspendLayout()
        CType(Me.lecturerDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.classesPanel.SuspendLayout()
        CType(Me.classDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sidebarSplitter
        '
        Me.sidebarSplitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.sidebarSplitter.Location = New System.Drawing.Point(0, 0)
        Me.sidebarSplitter.Name = "sidebarSplitter"
        Me.sidebarSplitter.Size = New System.Drawing.Size(177, 450)
        Me.sidebarSplitter.TabIndex = 4
        Me.sidebarSplitter.TabStop = False
        '
        'btnManageSubjects
        '
        Me.btnManageSubjects.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnManageSubjects.FlatAppearance.BorderSize = 0
        Me.btnManageSubjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageSubjects.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManageSubjects.ForeColor = System.Drawing.Color.White
        Me.btnManageSubjects.Location = New System.Drawing.Point(0, 114)
        Me.btnManageSubjects.Name = "btnManageSubjects"
        Me.btnManageSubjects.Size = New System.Drawing.Size(177, 23)
        Me.btnManageSubjects.TabIndex = 5
        Me.btnManageSubjects.Text = "Manage Subjects"
        Me.btnManageSubjects.UseVisualStyleBackColor = False
        '
        'btnManageLecturers
        '
        Me.btnManageLecturers.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnManageLecturers.FlatAppearance.BorderSize = 0
        Me.btnManageLecturers.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageLecturers.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManageLecturers.ForeColor = System.Drawing.Color.White
        Me.btnManageLecturers.Location = New System.Drawing.Point(0, 143)
        Me.btnManageLecturers.Name = "btnManageLecturers"
        Me.btnManageLecturers.Size = New System.Drawing.Size(177, 23)
        Me.btnManageLecturers.TabIndex = 6
        Me.btnManageLecturers.Text = "Manage Lecturers"
        Me.btnManageLecturers.UseVisualStyleBackColor = False
        '
        'mainIcon
        '
        Me.mainIcon.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(107, Byte), Integer))
        Me.mainIcon.Image = CType(resources.GetObject("mainIcon.Image"), System.Drawing.Image)
        Me.mainIcon.Location = New System.Drawing.Point(40, 9)
        Me.mainIcon.Name = "mainIcon"
        Me.mainIcon.Size = New System.Drawing.Size(91, 93)
        Me.mainIcon.TabIndex = 7
        '
        'btnManageClasses
        '
        Me.btnManageClasses.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnManageClasses.FlatAppearance.BorderSize = 0
        Me.btnManageClasses.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageClasses.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManageClasses.ForeColor = System.Drawing.Color.White
        Me.btnManageClasses.Location = New System.Drawing.Point(0, 172)
        Me.btnManageClasses.Name = "btnManageClasses"
        Me.btnManageClasses.Size = New System.Drawing.Size(177, 23)
        Me.btnManageClasses.TabIndex = 8
        Me.btnManageClasses.Text = "Manage Classes"
        Me.btnManageClasses.UseVisualStyleBackColor = False
        '
        'btnManageStudentsGPA
        '
        Me.btnManageStudentsGPA.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnManageStudentsGPA.FlatAppearance.BorderSize = 0
        Me.btnManageStudentsGPA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageStudentsGPA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManageStudentsGPA.ForeColor = System.Drawing.Color.White
        Me.btnManageStudentsGPA.Location = New System.Drawing.Point(0, 201)
        Me.btnManageStudentsGPA.Name = "btnManageStudentsGPA"
        Me.btnManageStudentsGPA.Size = New System.Drawing.Size(177, 23)
        Me.btnManageStudentsGPA.TabIndex = 9
        Me.btnManageStudentsGPA.Text = "Manage Students GPA"
        Me.btnManageStudentsGPA.UseVisualStyleBackColor = False
        '
        'subjectPanel
        '
        Me.subjectPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.subjectPanel.Controls.Add(Me.subjectDataGrid)
        Me.subjectPanel.Controls.Add(Me.btnEditCourse)
        Me.subjectPanel.Controls.Add(Me.btnAddCourse)
        Me.subjectPanel.Controls.Add(Me.btnDeleteCourse)
        Me.subjectPanel.Controls.Add(Me.btnUpdateCourse)
        Me.subjectPanel.Controls.Add(Me.btnLoadCourse)
        Me.subjectPanel.Controls.Add(Me.btnClearAllCourse)
        Me.subjectPanel.Controls.Add(Me.actionSplitter)
        Me.subjectPanel.Controls.Add(Me.txtSubjectDescription)
        Me.subjectPanel.Controls.Add(Me.lblSubjectDescription)
        Me.subjectPanel.Controls.Add(Me.lblSubjectName)
        Me.subjectPanel.Controls.Add(Me.txtSubjectName)
        Me.subjectPanel.Controls.Add(Me.lblSubjectID)
        Me.subjectPanel.Controls.Add(Me.txtSubjectID)
        Me.subjectPanel.Location = New System.Drawing.Point(183, 0)
        Me.subjectPanel.Name = "subjectPanel"
        Me.subjectPanel.Size = New System.Drawing.Size(612, 450)
        Me.subjectPanel.TabIndex = 10
        '
        'subjectDataGrid
        '
        Me.subjectDataGrid.AllowUserToAddRows = False
        Me.subjectDataGrid.AllowUserToDeleteRows = False
        Me.subjectDataGrid.BackgroundColor = System.Drawing.Color.White
        Me.subjectDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.subjectDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.subjectDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.subject_id, Me.subject_name, Me.subject_description})
        Me.subjectDataGrid.Location = New System.Drawing.Point(3, 242)
        Me.subjectDataGrid.Name = "subjectDataGrid"
        Me.subjectDataGrid.ReadOnly = True
        Me.subjectDataGrid.Size = New System.Drawing.Size(606, 111)
        Me.subjectDataGrid.TabIndex = 17
        '
        'subject_id
        '
        Me.subject_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.subject_id.HeaderText = "Subject ID"
        Me.subject_id.Name = "subject_id"
        Me.subject_id.ReadOnly = True
        Me.subject_id.Width = 76
        '
        'subject_name
        '
        Me.subject_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.subject_name.HeaderText = "Subject Name"
        Me.subject_name.Name = "subject_name"
        Me.subject_name.ReadOnly = True
        Me.subject_name.Width = 91
        '
        'subject_description
        '
        Me.subject_description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.subject_description.HeaderText = "Subject Description"
        Me.subject_description.Name = "subject_description"
        Me.subject_description.ReadOnly = True
        '
        'btnEditCourse
        '
        Me.btnEditCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnEditCourse.FlatAppearance.BorderSize = 0
        Me.btnEditCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditCourse.ForeColor = System.Drawing.Color.White
        Me.btnEditCourse.Location = New System.Drawing.Point(401, 408)
        Me.btnEditCourse.Name = "btnEditCourse"
        Me.btnEditCourse.Size = New System.Drawing.Size(177, 23)
        Me.btnEditCourse.TabIndex = 16
        Me.btnEditCourse.Text = "Save"
        Me.btnEditCourse.UseVisualStyleBackColor = False
        '
        'btnAddCourse
        '
        Me.btnAddCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnAddCourse.FlatAppearance.BorderSize = 0
        Me.btnAddCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddCourse.ForeColor = System.Drawing.Color.White
        Me.btnAddCourse.Location = New System.Drawing.Point(218, 408)
        Me.btnAddCourse.Name = "btnAddCourse"
        Me.btnAddCourse.Size = New System.Drawing.Size(177, 23)
        Me.btnAddCourse.TabIndex = 15
        Me.btnAddCourse.Text = "Add Course"
        Me.btnAddCourse.UseVisualStyleBackColor = False
        '
        'btnDeleteCourse
        '
        Me.btnDeleteCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnDeleteCourse.FlatAppearance.BorderSize = 0
        Me.btnDeleteCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteCourse.ForeColor = System.Drawing.Color.White
        Me.btnDeleteCourse.Location = New System.Drawing.Point(35, 408)
        Me.btnDeleteCourse.Name = "btnDeleteCourse"
        Me.btnDeleteCourse.Size = New System.Drawing.Size(177, 23)
        Me.btnDeleteCourse.TabIndex = 14
        Me.btnDeleteCourse.Text = "Delete Course"
        Me.btnDeleteCourse.UseVisualStyleBackColor = False
        '
        'btnUpdateCourse
        '
        Me.btnUpdateCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnUpdateCourse.FlatAppearance.BorderSize = 0
        Me.btnUpdateCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdateCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateCourse.ForeColor = System.Drawing.Color.White
        Me.btnUpdateCourse.Location = New System.Drawing.Point(401, 379)
        Me.btnUpdateCourse.Name = "btnUpdateCourse"
        Me.btnUpdateCourse.Size = New System.Drawing.Size(177, 23)
        Me.btnUpdateCourse.TabIndex = 13
        Me.btnUpdateCourse.Text = "Update Course"
        Me.btnUpdateCourse.UseVisualStyleBackColor = False
        '
        'btnLoadCourse
        '
        Me.btnLoadCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnLoadCourse.FlatAppearance.BorderSize = 0
        Me.btnLoadCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoadCourse.ForeColor = System.Drawing.Color.White
        Me.btnLoadCourse.Location = New System.Drawing.Point(218, 379)
        Me.btnLoadCourse.Name = "btnLoadCourse"
        Me.btnLoadCourse.Size = New System.Drawing.Size(177, 23)
        Me.btnLoadCourse.TabIndex = 12
        Me.btnLoadCourse.Text = "Load Course"
        Me.btnLoadCourse.UseVisualStyleBackColor = False
        '
        'btnClearAllCourse
        '
        Me.btnClearAllCourse.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnClearAllCourse.FlatAppearance.BorderSize = 0
        Me.btnClearAllCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearAllCourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearAllCourse.ForeColor = System.Drawing.Color.White
        Me.btnClearAllCourse.Location = New System.Drawing.Point(35, 379)
        Me.btnClearAllCourse.Name = "btnClearAllCourse"
        Me.btnClearAllCourse.Size = New System.Drawing.Size(177, 23)
        Me.btnClearAllCourse.TabIndex = 11
        Me.btnClearAllCourse.Text = "Clear All"
        Me.btnClearAllCourse.UseVisualStyleBackColor = False
        '
        'actionSplitter
        '
        Me.actionSplitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.actionSplitter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.actionSplitter.Location = New System.Drawing.Point(0, 359)
        Me.actionSplitter.Name = "actionSplitter"
        Me.actionSplitter.Size = New System.Drawing.Size(612, 91)
        Me.actionSplitter.TabIndex = 6
        Me.actionSplitter.TabStop = False
        '
        'txtSubjectDescription
        '
        Me.txtSubjectDescription.Location = New System.Drawing.Point(127, 114)
        Me.txtSubjectDescription.Multiline = True
        Me.txtSubjectDescription.Name = "txtSubjectDescription"
        Me.txtSubjectDescription.Size = New System.Drawing.Size(464, 79)
        Me.txtSubjectDescription.TabIndex = 5
        '
        'lblSubjectDescription
        '
        Me.lblSubjectDescription.AutoSize = True
        Me.lblSubjectDescription.Location = New System.Drawing.Point(22, 117)
        Me.lblSubjectDescription.Name = "lblSubjectDescription"
        Me.lblSubjectDescription.Size = New System.Drawing.Size(102, 13)
        Me.lblSubjectDescription.TabIndex = 4
        Me.lblSubjectDescription.Text = "Subject Description:"
        '
        'lblSubjectName
        '
        Me.lblSubjectName.AutoSize = True
        Me.lblSubjectName.Location = New System.Drawing.Point(22, 90)
        Me.lblSubjectName.Name = "lblSubjectName"
        Me.lblSubjectName.Size = New System.Drawing.Size(77, 13)
        Me.lblSubjectName.TabIndex = 3
        Me.lblSubjectName.Text = "Subject Name:"
        '
        'txtSubjectName
        '
        Me.txtSubjectName.Location = New System.Drawing.Point(127, 87)
        Me.txtSubjectName.Name = "txtSubjectName"
        Me.txtSubjectName.Size = New System.Drawing.Size(203, 20)
        Me.txtSubjectName.TabIndex = 2
        '
        'lblSubjectID
        '
        Me.lblSubjectID.AutoSize = True
        Me.lblSubjectID.Location = New System.Drawing.Point(22, 64)
        Me.lblSubjectID.Name = "lblSubjectID"
        Me.lblSubjectID.Size = New System.Drawing.Size(60, 13)
        Me.lblSubjectID.TabIndex = 1
        Me.lblSubjectID.Text = "Subject ID:"
        '
        'txtSubjectID
        '
        Me.txtSubjectID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubjectID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSubjectID.Location = New System.Drawing.Point(127, 61)
        Me.txtSubjectID.Name = "txtSubjectID"
        Me.txtSubjectID.Size = New System.Drawing.Size(203, 20)
        Me.txtSubjectID.TabIndex = 0
        '
        'CcourseBindingSource1
        '
        Me.CcourseBindingSource1.DataMember = "c_course"
        Me.CcourseBindingSource1.DataSource = Me.Student_Grading_System_DataSet1
        '
        'Student_Grading_System_DataSet1
        '
        Me.Student_Grading_System_DataSet1.DataSetName = "Student_Grading_System_DataSet"
        Me.Student_Grading_System_DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Student_Grading_System_DataSet
        '
        Me.Student_Grading_System_DataSet.DataSetName = "Student_Grading_System_DataSet"
        Me.Student_Grading_System_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'C_courseTableAdapter
        '
        Me.C_courseTableAdapter.ClearBeforeFill = True
        '
        'lecturerPanel
        '
        Me.lecturerPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.lecturerPanel.Controls.Add(Me.txtLecturerEmail)
        Me.lecturerPanel.Controls.Add(Me.lblLecturerEmail)
        Me.lecturerPanel.Controls.Add(Me.txtLecturerContact)
        Me.lecturerPanel.Controls.Add(Me.lecturerDataGrid)
        Me.lecturerPanel.Controls.Add(Me.Button1)
        Me.lecturerPanel.Controls.Add(Me.btnAddLecturer)
        Me.lecturerPanel.Controls.Add(Me.btnDeleteLecturer)
        Me.lecturerPanel.Controls.Add(Me.btnUpdateLecturer)
        Me.lecturerPanel.Controls.Add(Me.btnLoadLecturer)
        Me.lecturerPanel.Controls.Add(Me.btnClearAllLecturer)
        Me.lecturerPanel.Controls.Add(Me.Splitter1)
        Me.lecturerPanel.Controls.Add(Me.lblLecturerContact)
        Me.lecturerPanel.Controls.Add(Me.lblLecturerName)
        Me.lecturerPanel.Controls.Add(Me.txtLecturerName)
        Me.lecturerPanel.Controls.Add(Me.lblLecturerID)
        Me.lecturerPanel.Controls.Add(Me.txtLecturerID)
        Me.lecturerPanel.Location = New System.Drawing.Point(183, 0)
        Me.lecturerPanel.Name = "lecturerPanel"
        Me.lecturerPanel.Size = New System.Drawing.Size(612, 450)
        Me.lecturerPanel.TabIndex = 11
        '
        'txtLecturerEmail
        '
        Me.txtLecturerEmail.Location = New System.Drawing.Point(130, 140)
        Me.txtLecturerEmail.Name = "txtLecturerEmail"
        Me.txtLecturerEmail.Size = New System.Drawing.Size(203, 20)
        Me.txtLecturerEmail.TabIndex = 20
        '
        'lblLecturerEmail
        '
        Me.lblLecturerEmail.AutoSize = True
        Me.lblLecturerEmail.Location = New System.Drawing.Point(22, 143)
        Me.lblLecturerEmail.Name = "lblLecturerEmail"
        Me.lblLecturerEmail.Size = New System.Drawing.Size(77, 13)
        Me.lblLecturerEmail.TabIndex = 19
        Me.lblLecturerEmail.Text = "Lecturer Email:"
        '
        'txtLecturerContact
        '
        Me.txtLecturerContact.Location = New System.Drawing.Point(130, 114)
        Me.txtLecturerContact.Name = "txtLecturerContact"
        Me.txtLecturerContact.Size = New System.Drawing.Size(203, 20)
        Me.txtLecturerContact.TabIndex = 18
        '
        'lecturerDataGrid
        '
        Me.lecturerDataGrid.AllowUserToAddRows = False
        Me.lecturerDataGrid.AllowUserToDeleteRows = False
        Me.lecturerDataGrid.BackgroundColor = System.Drawing.Color.White
        Me.lecturerDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lecturerDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.lecturerDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.lecturer_id, Me.lecturer_name, Me.lecturer_contact, Me.lecturer_email})
        Me.lecturerDataGrid.Location = New System.Drawing.Point(3, 242)
        Me.lecturerDataGrid.Name = "lecturerDataGrid"
        Me.lecturerDataGrid.ReadOnly = True
        Me.lecturerDataGrid.Size = New System.Drawing.Size(606, 111)
        Me.lecturerDataGrid.TabIndex = 17
        '
        'lecturer_id
        '
        Me.lecturer_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.lecturer_id.HeaderText = "Lecturer ID"
        Me.lecturer_id.Name = "lecturer_id"
        Me.lecturer_id.ReadOnly = True
        Me.lecturer_id.Width = 78
        '
        'lecturer_name
        '
        Me.lecturer_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.lecturer_name.HeaderText = "Lecturer Name"
        Me.lecturer_name.Name = "lecturer_name"
        Me.lecturer_name.ReadOnly = True
        Me.lecturer_name.Width = 94
        '
        'lecturer_contact
        '
        Me.lecturer_contact.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.lecturer_contact.HeaderText = "Lecturer Contact"
        Me.lecturer_contact.Name = "lecturer_contact"
        Me.lecturer_contact.ReadOnly = True
        Me.lecturer_contact.Width = 102
        '
        'lecturer_email
        '
        Me.lecturer_email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lecturer_email.HeaderText = "Lecturer Email"
        Me.lecturer_email.Name = "lecturer_email"
        Me.lecturer_email.ReadOnly = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(401, 408)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(177, 23)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnAddLecturer
        '
        Me.btnAddLecturer.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnAddLecturer.FlatAppearance.BorderSize = 0
        Me.btnAddLecturer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddLecturer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddLecturer.ForeColor = System.Drawing.Color.White
        Me.btnAddLecturer.Location = New System.Drawing.Point(218, 408)
        Me.btnAddLecturer.Name = "btnAddLecturer"
        Me.btnAddLecturer.Size = New System.Drawing.Size(177, 23)
        Me.btnAddLecturer.TabIndex = 15
        Me.btnAddLecturer.Text = "Add Lecturer"
        Me.btnAddLecturer.UseVisualStyleBackColor = False
        '
        'btnDeleteLecturer
        '
        Me.btnDeleteLecturer.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnDeleteLecturer.FlatAppearance.BorderSize = 0
        Me.btnDeleteLecturer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteLecturer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteLecturer.ForeColor = System.Drawing.Color.White
        Me.btnDeleteLecturer.Location = New System.Drawing.Point(35, 408)
        Me.btnDeleteLecturer.Name = "btnDeleteLecturer"
        Me.btnDeleteLecturer.Size = New System.Drawing.Size(177, 23)
        Me.btnDeleteLecturer.TabIndex = 14
        Me.btnDeleteLecturer.Text = "Delete Lecturer"
        Me.btnDeleteLecturer.UseVisualStyleBackColor = False
        '
        'btnUpdateLecturer
        '
        Me.btnUpdateLecturer.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnUpdateLecturer.FlatAppearance.BorderSize = 0
        Me.btnUpdateLecturer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdateLecturer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateLecturer.ForeColor = System.Drawing.Color.White
        Me.btnUpdateLecturer.Location = New System.Drawing.Point(401, 379)
        Me.btnUpdateLecturer.Name = "btnUpdateLecturer"
        Me.btnUpdateLecturer.Size = New System.Drawing.Size(177, 23)
        Me.btnUpdateLecturer.TabIndex = 13
        Me.btnUpdateLecturer.Text = "Update Lecturer"
        Me.btnUpdateLecturer.UseVisualStyleBackColor = False
        '
        'btnLoadLecturer
        '
        Me.btnLoadLecturer.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnLoadLecturer.FlatAppearance.BorderSize = 0
        Me.btnLoadLecturer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadLecturer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoadLecturer.ForeColor = System.Drawing.Color.White
        Me.btnLoadLecturer.Location = New System.Drawing.Point(218, 379)
        Me.btnLoadLecturer.Name = "btnLoadLecturer"
        Me.btnLoadLecturer.Size = New System.Drawing.Size(177, 23)
        Me.btnLoadLecturer.TabIndex = 12
        Me.btnLoadLecturer.Text = "Load Lecturer"
        Me.btnLoadLecturer.UseVisualStyleBackColor = False
        '
        'btnClearAllLecturer
        '
        Me.btnClearAllLecturer.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnClearAllLecturer.FlatAppearance.BorderSize = 0
        Me.btnClearAllLecturer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearAllLecturer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearAllLecturer.ForeColor = System.Drawing.Color.White
        Me.btnClearAllLecturer.Location = New System.Drawing.Point(35, 379)
        Me.btnClearAllLecturer.Name = "btnClearAllLecturer"
        Me.btnClearAllLecturer.Size = New System.Drawing.Size(177, 23)
        Me.btnClearAllLecturer.TabIndex = 11
        Me.btnClearAllLecturer.Text = "Clear All"
        Me.btnClearAllLecturer.UseVisualStyleBackColor = False
        '
        'Splitter1
        '
        Me.Splitter1.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter1.Location = New System.Drawing.Point(0, 359)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(612, 91)
        Me.Splitter1.TabIndex = 6
        Me.Splitter1.TabStop = False
        '
        'lblLecturerContact
        '
        Me.lblLecturerContact.AutoSize = True
        Me.lblLecturerContact.Location = New System.Drawing.Point(22, 117)
        Me.lblLecturerContact.Name = "lblLecturerContact"
        Me.lblLecturerContact.Size = New System.Drawing.Size(89, 13)
        Me.lblLecturerContact.TabIndex = 4
        Me.lblLecturerContact.Text = "Lecturer Contact:"
        '
        'lblLecturerName
        '
        Me.lblLecturerName.AutoSize = True
        Me.lblLecturerName.Location = New System.Drawing.Point(22, 90)
        Me.lblLecturerName.Name = "lblLecturerName"
        Me.lblLecturerName.Size = New System.Drawing.Size(80, 13)
        Me.lblLecturerName.TabIndex = 3
        Me.lblLecturerName.Text = "Lecturer Name:"
        '
        'txtLecturerName
        '
        Me.txtLecturerName.Location = New System.Drawing.Point(130, 87)
        Me.txtLecturerName.Name = "txtLecturerName"
        Me.txtLecturerName.Size = New System.Drawing.Size(203, 20)
        Me.txtLecturerName.TabIndex = 2
        '
        'lblLecturerID
        '
        Me.lblLecturerID.AutoSize = True
        Me.lblLecturerID.Location = New System.Drawing.Point(22, 63)
        Me.lblLecturerID.Name = "lblLecturerID"
        Me.lblLecturerID.Size = New System.Drawing.Size(63, 13)
        Me.lblLecturerID.TabIndex = 1
        Me.lblLecturerID.Text = "Lecturer ID:"
        '
        'txtLecturerID
        '
        Me.txtLecturerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLecturerID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLecturerID.Location = New System.Drawing.Point(130, 61)
        Me.txtLecturerID.Name = "txtLecturerID"
        Me.txtLecturerID.Size = New System.Drawing.Size(203, 20)
        Me.txtLecturerID.TabIndex = 0
        '
        'classesPanel
        '
        Me.classesPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.classesPanel.Controls.Add(Me.cboLecturer)
        Me.classesPanel.Controls.Add(Me.cboSubjectName)
        Me.classesPanel.Controls.Add(Me.Label1)
        Me.classesPanel.Controls.Add(Me.txtCreditHour)
        Me.classesPanel.Controls.Add(Me.classDataGrid)
        Me.classesPanel.Controls.Add(Me.btnSaveClass)
        Me.classesPanel.Controls.Add(Me.btnAddClass)
        Me.classesPanel.Controls.Add(Me.btnDeleteClass)
        Me.classesPanel.Controls.Add(Me.btnUpdateClass)
        Me.classesPanel.Controls.Add(Me.btnLoadClass)
        Me.classesPanel.Controls.Add(Me.btnClearAllClass)
        Me.classesPanel.Controls.Add(Me.Splitter2)
        Me.classesPanel.Controls.Add(Me.Label2)
        Me.classesPanel.Controls.Add(Me.Label3)
        Me.classesPanel.Controls.Add(Me.Label4)
        Me.classesPanel.Controls.Add(Me.txtClassID)
        Me.classesPanel.Location = New System.Drawing.Point(183, 0)
        Me.classesPanel.Name = "classesPanel"
        Me.classesPanel.Size = New System.Drawing.Size(612, 450)
        Me.classesPanel.TabIndex = 12
        '
        'cboLecturer
        '
        Me.cboLecturer.FormattingEnabled = True
        Me.cboLecturer.Location = New System.Drawing.Point(130, 140)
        Me.cboLecturer.Name = "cboLecturer"
        Me.cboLecturer.Size = New System.Drawing.Size(203, 21)
        Me.cboLecturer.TabIndex = 22
        '
        'cboSubjectName
        '
        Me.cboSubjectName.FormattingEnabled = True
        Me.cboSubjectName.Location = New System.Drawing.Point(130, 87)
        Me.cboSubjectName.Name = "cboSubjectName"
        Me.cboSubjectName.Size = New System.Drawing.Size(203, 21)
        Me.cboSubjectName.TabIndex = 21
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 143)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Lecturer:"
        '
        'txtCreditHour
        '
        Me.txtCreditHour.Location = New System.Drawing.Point(130, 114)
        Me.txtCreditHour.Name = "txtCreditHour"
        Me.txtCreditHour.Size = New System.Drawing.Size(203, 20)
        Me.txtCreditHour.TabIndex = 18
        '
        'classDataGrid
        '
        Me.classDataGrid.AllowUserToAddRows = False
        Me.classDataGrid.AllowUserToDeleteRows = False
        Me.classDataGrid.BackgroundColor = System.Drawing.Color.White
        Me.classDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.classDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.classDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.class_id, Me.subject_name_classes, Me.credit_hour, Me.lecturer})
        Me.classDataGrid.Location = New System.Drawing.Point(3, 242)
        Me.classDataGrid.Name = "classDataGrid"
        Me.classDataGrid.ReadOnly = True
        Me.classDataGrid.Size = New System.Drawing.Size(606, 111)
        Me.classDataGrid.TabIndex = 17
        '
        'class_id
        '
        Me.class_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.class_id.HeaderText = "Class ID"
        Me.class_id.Name = "class_id"
        Me.class_id.ReadOnly = True
        Me.class_id.Width = 71
        '
        'subject_name_classes
        '
        Me.subject_name_classes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.subject_name_classes.HeaderText = "Subject Name"
        Me.subject_name_classes.Name = "subject_name_classes"
        Me.subject_name_classes.ReadOnly = True
        Me.subject_name_classes.Width = 99
        '
        'credit_hour
        '
        Me.credit_hour.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.credit_hour.HeaderText = "Credit Hour"
        Me.credit_hour.Name = "credit_hour"
        Me.credit_hour.ReadOnly = True
        Me.credit_hour.Width = 85
        '
        'lecturer
        '
        Me.lecturer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lecturer.HeaderText = "Lecturer"
        Me.lecturer.Name = "lecturer"
        Me.lecturer.ReadOnly = True
        '
        'btnSaveClass
        '
        Me.btnSaveClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnSaveClass.FlatAppearance.BorderSize = 0
        Me.btnSaveClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveClass.ForeColor = System.Drawing.Color.White
        Me.btnSaveClass.Location = New System.Drawing.Point(401, 408)
        Me.btnSaveClass.Name = "btnSaveClass"
        Me.btnSaveClass.Size = New System.Drawing.Size(177, 23)
        Me.btnSaveClass.TabIndex = 16
        Me.btnSaveClass.Text = "Save"
        Me.btnSaveClass.UseVisualStyleBackColor = False
        '
        'btnAddClass
        '
        Me.btnAddClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnAddClass.FlatAppearance.BorderSize = 0
        Me.btnAddClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddClass.ForeColor = System.Drawing.Color.White
        Me.btnAddClass.Location = New System.Drawing.Point(218, 408)
        Me.btnAddClass.Name = "btnAddClass"
        Me.btnAddClass.Size = New System.Drawing.Size(177, 23)
        Me.btnAddClass.TabIndex = 15
        Me.btnAddClass.Text = "Add Class"
        Me.btnAddClass.UseVisualStyleBackColor = False
        '
        'btnDeleteClass
        '
        Me.btnDeleteClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnDeleteClass.FlatAppearance.BorderSize = 0
        Me.btnDeleteClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteClass.ForeColor = System.Drawing.Color.White
        Me.btnDeleteClass.Location = New System.Drawing.Point(35, 408)
        Me.btnDeleteClass.Name = "btnDeleteClass"
        Me.btnDeleteClass.Size = New System.Drawing.Size(177, 23)
        Me.btnDeleteClass.TabIndex = 14
        Me.btnDeleteClass.Text = "Delete Class"
        Me.btnDeleteClass.UseVisualStyleBackColor = False
        '
        'btnUpdateClass
        '
        Me.btnUpdateClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnUpdateClass.FlatAppearance.BorderSize = 0
        Me.btnUpdateClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdateClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateClass.ForeColor = System.Drawing.Color.White
        Me.btnUpdateClass.Location = New System.Drawing.Point(401, 379)
        Me.btnUpdateClass.Name = "btnUpdateClass"
        Me.btnUpdateClass.Size = New System.Drawing.Size(177, 23)
        Me.btnUpdateClass.TabIndex = 13
        Me.btnUpdateClass.Text = "Update Class"
        Me.btnUpdateClass.UseVisualStyleBackColor = False
        '
        'btnLoadClass
        '
        Me.btnLoadClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnLoadClass.FlatAppearance.BorderSize = 0
        Me.btnLoadClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoadClass.ForeColor = System.Drawing.Color.White
        Me.btnLoadClass.Location = New System.Drawing.Point(218, 379)
        Me.btnLoadClass.Name = "btnLoadClass"
        Me.btnLoadClass.Size = New System.Drawing.Size(177, 23)
        Me.btnLoadClass.TabIndex = 12
        Me.btnLoadClass.Text = "Load Class"
        Me.btnLoadClass.UseVisualStyleBackColor = False
        '
        'btnClearAllClass
        '
        Me.btnClearAllClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnClearAllClass.FlatAppearance.BorderSize = 0
        Me.btnClearAllClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearAllClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearAllClass.ForeColor = System.Drawing.Color.White
        Me.btnClearAllClass.Location = New System.Drawing.Point(35, 379)
        Me.btnClearAllClass.Name = "btnClearAllClass"
        Me.btnClearAllClass.Size = New System.Drawing.Size(177, 23)
        Me.btnClearAllClass.TabIndex = 11
        Me.btnClearAllClass.Text = "Clear All"
        Me.btnClearAllClass.UseVisualStyleBackColor = False
        '
        'Splitter2
        '
        Me.Splitter2.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter2.Location = New System.Drawing.Point(0, 359)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(612, 91)
        Me.Splitter2.TabIndex = 6
        Me.Splitter2.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Credit Hour:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Subject Name:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Class ID:"
        '
        'txtClassID
        '
        Me.txtClassID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClassID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClassID.Location = New System.Drawing.Point(130, 61)
        Me.txtClassID.Name = "txtClassID"
        Me.txtClassID.Size = New System.Drawing.Size(203, 20)
        Me.txtClassID.TabIndex = 0
        '
        'btnManageStudentsCGPA
        '
        Me.btnManageStudentsCGPA.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnManageStudentsCGPA.FlatAppearance.BorderSize = 0
        Me.btnManageStudentsCGPA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageStudentsCGPA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManageStudentsCGPA.ForeColor = System.Drawing.Color.White
        Me.btnManageStudentsCGPA.Location = New System.Drawing.Point(0, 230)
        Me.btnManageStudentsCGPA.Name = "btnManageStudentsCGPA"
        Me.btnManageStudentsCGPA.Size = New System.Drawing.Size(177, 23)
        Me.btnManageStudentsCGPA.TabIndex = 13
        Me.btnManageStudentsCGPA.Text = "Manage Students CGPA"
        Me.btnManageStudentsCGPA.UseVisualStyleBackColor = False
        '
        'Student_Grading_System
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnManageStudentsCGPA)
        Me.Controls.Add(Me.btnManageStudentsGPA)
        Me.Controls.Add(Me.btnManageClasses)
        Me.Controls.Add(Me.mainIcon)
        Me.Controls.Add(Me.btnManageLecturers)
        Me.Controls.Add(Me.btnManageSubjects)
        Me.Controls.Add(Me.sidebarSplitter)
        Me.Controls.Add(Me.classesPanel)
        Me.Controls.Add(Me.lecturerPanel)
        Me.Controls.Add(Me.subjectPanel)
        Me.Name = "Student_Grading_System"
        Me.Text = "Student Grading System"
        Me.subjectPanel.ResumeLayout(False)
        Me.subjectPanel.PerformLayout()
        CType(Me.subjectDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CcourseBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Student_Grading_System_DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Student_Grading_System_DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CcourseBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lecturerPanel.ResumeLayout(False)
        Me.lecturerPanel.PerformLayout()
        CType(Me.lecturerDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.classesPanel.ResumeLayout(False)
        Me.classesPanel.PerformLayout()
        CType(Me.classDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sidebarSplitter As Splitter
    Friend WithEvents btnManageSubjects As Button
    Friend WithEvents btnManageLecturers As Button
    Friend WithEvents mainIcon As Label
    Friend WithEvents btnManageClasses As Button
    Friend WithEvents btnManageStudentsGPA As Button
    Friend WithEvents lblSubjectID As Label
    Friend WithEvents txtSubjectID As TextBox
    Friend WithEvents txtSubjectDescription As TextBox
    Friend WithEvents lblSubjectDescription As Label
    Friend WithEvents lblSubjectName As Label
    Friend WithEvents txtSubjectName As TextBox
    Friend WithEvents actionSplitter As Splitter
    Friend WithEvents subjectPanel As Panel
    Friend WithEvents btnEditCourse As Button
    Friend WithEvents btnAddCourse As Button
    Friend WithEvents btnDeleteCourse As Button
    Friend WithEvents btnUpdateCourse As Button
    Friend WithEvents btnLoadCourse As Button
    Friend WithEvents btnClearAllCourse As Button
    Friend WithEvents subjectDataGrid As DataGridView
    Friend WithEvents Student_Grading_System_DataSet As Student_Grading_System_DataSet
    Friend WithEvents CcourseBindingSource As BindingSource
    Friend WithEvents C_courseTableAdapter As Student_Grading_System_DataSetTableAdapters.c_courseTableAdapter
    Friend WithEvents CourseidDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CoursenameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CoursedescriptionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Student_Grading_System_DataSet1 As Student_Grading_System_DataSet
    Friend WithEvents CcourseBindingSource1 As BindingSource
    Friend WithEvents subject_id As DataGridViewTextBoxColumn
    Friend WithEvents subject_name As DataGridViewTextBoxColumn
    Friend WithEvents subject_description As DataGridViewTextBoxColumn
    Friend WithEvents lecturerPanel As Panel
    Friend WithEvents lecturerDataGrid As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents btnAddLecturer As Button
    Friend WithEvents btnDeleteLecturer As Button
    Friend WithEvents btnUpdateLecturer As Button
    Friend WithEvents btnLoadLecturer As Button
    Friend WithEvents btnClearAllLecturer As Button
    Friend WithEvents Splitter1 As Splitter
    Friend WithEvents lblLecturerContact As Label
    Friend WithEvents lblLecturerName As Label
    Friend WithEvents lblLecturerID As Label
    Friend WithEvents txtLecturerID As TextBox
    Friend WithEvents lblLecturerEmail As Label
    Friend WithEvents txtLecturerContact As TextBox
    Friend WithEvents txtLecturerEmail As TextBox
    Friend WithEvents lecturer_id As DataGridViewTextBoxColumn
    Friend WithEvents lecturer_name As DataGridViewTextBoxColumn
    Friend WithEvents lecturer_contact As DataGridViewTextBoxColumn
    Friend WithEvents lecturer_email As DataGridViewTextBoxColumn
    Friend WithEvents txtLecturerName As TextBox
    Friend WithEvents classesPanel As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCreditHour As TextBox
    Friend WithEvents classDataGrid As DataGridView
    Friend WithEvents btnSaveClass As Button
    Friend WithEvents btnAddClass As Button
    Friend WithEvents btnDeleteClass As Button
    Friend WithEvents btnUpdateClass As Button
    Friend WithEvents btnLoadClass As Button
    Friend WithEvents btnClearAllClass As Button
    Friend WithEvents Splitter2 As Splitter
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtClassID As TextBox
    Friend WithEvents cboLecturer As ComboBox
    Friend WithEvents cboSubjectName As ComboBox
    Friend WithEvents class_id As DataGridViewTextBoxColumn
    Friend WithEvents subject_name_classes As DataGridViewTextBoxColumn
    Friend WithEvents credit_hour As DataGridViewTextBoxColumn
    Friend WithEvents lecturer As DataGridViewTextBoxColumn
    Friend WithEvents btnManageStudentsCGPA As Button
End Class
