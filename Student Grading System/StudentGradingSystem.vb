﻿Public Class Student_Grading_System
    Private Sub Student_Grading_System_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Student_Grading_System_DataSet1.c_course' table. You can move, or remove it, as needed.
        Me.C_courseTableAdapter.Fill(Me.Student_Grading_System_DataSet1.c_course)
        'TODO: This line of code loads data into the 'Student_Grading_System_DataSet.c_course' table. You can move, or remove it, as needed.
        Me.C_courseTableAdapter.Fill(Me.Student_Grading_System_DataSet.c_course)
        'TODO: This line of code loads data into the 'Student_Grading_System_DataSet.c_course' table. You can move, or remove it, as needed.
        Me.C_courseTableAdapter.Fill(Me.Student_Grading_System_DataSet.c_course)

    End Sub

    Private Sub btnClearAll_Click(sender As Object, e As EventArgs) Handles btnClearAllCourse.Click
        txtSubjectID.Clear()
        txtSubjectName.Clear()
        txtSubjectDescription.Clear()
    End Sub

    Private Sub btnLoadCourse_Click(sender As Object, e As EventArgs) Handles btnLoadCourse.Click
        Try
            txtSubjectID.Text = subjectDataGrid.SelectedRows(0).Cells(0).Value
            txtSubjectName.Text = subjectDataGrid.SelectedRows(0).Cells(1).Value
            txtSubjectDescription.Text = subjectDataGrid.SelectedRows(0).Cells(2).Value
        Catch err As System.ArgumentOutOfRangeException
            MsgBox("You must select a row instead of a cell!", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btnUpdateCourse_Click(sender As Object, e As EventArgs) Handles btnUpdateCourse.Click
        'Error Checking
        If txtSubjectID.Text.Equals("") Then
            MsgBox("Subject ID cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtSubjectName.Text.Equals("") Then
            MsgBox("Subject Name cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtSubjectDescription.Text.Equals("") Then
            MsgBox("Subject Description cannot be left empty!", MsgBoxStyle.Critical)
        End If

        Dim courseID As Integer
        Try
            courseID = CInt(txtSubjectID.Text)
        Catch err As System.InvalidCastException
            MsgBox("Subject ID cannot be a text value!", MsgBoxStyle.Critical)
        End Try

        Dim foundCourseID As Boolean = False
        For i As Integer = 0 To (subjectDataGrid.Rows.Count - 1)
            If CInt(subjectDataGrid.Rows(i).Cells(0).Value) = courseID Then
                foundCourseID = True
            End If
        Next

        If foundCourseID Then
            subjectDataGrid.Rows(courseID - 1).Cells(1).Value = txtSubjectName.Text
            subjectDataGrid.Rows(courseID - 1).Cells(2).Value = txtSubjectDescription.Text
        Else
            MsgBox("Subject ID does not exist!", MsgBoxStyle.Critical)
        End If
        'End of Error Checking'
    End Sub

    Private Sub btnDeleteCourse_Click(sender As Object, e As EventArgs) Handles btnDeleteCourse.Click
        For Each row As DataGridViewRow In subjectDataGrid.SelectedRows
            subjectDataGrid.Rows.Remove(row)
        Next
    End Sub

    Private Sub btnAddCourse_Click(sender As Object, e As EventArgs) Handles btnAddCourse.Click
        If txtSubjectID.Text.Equals("") Then
            MsgBox("Subject ID cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtSubjectName.Text.Equals("") Then
            MsgBox("Subject Name cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtSubjectDescription.Text.Equals("") Then
            MsgBox("Subject Description cannot be left empty!", MsgBoxStyle.Critical)
        End If

        Dim subjectID As Integer = CInt(txtSubjectID.Text)
        Dim subjectName As String = CStr(txtSubjectName.Text)
        Dim subjectDescription As String = CStr(txtSubjectDescription.Text)

        Dim foundCourseID As Boolean = False
        For i As Integer = 0 To (subjectDataGrid.Rows.Count - 1)
            If CInt(subjectDataGrid.Rows(i).Cells(0).Value) = subjectID Then
                foundCourseID = True
            End If
        Next

        If foundCourseID Then
            MsgBox("Subject ID already exist!", MsgBoxStyle.Critical)
        Else
            subjectDataGrid.Rows.Add({subjectID, subjectName, subjectDescription})
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles btnClearAllLecturer.Click
        txtLecturerID.Clear()
        txtLecturerName.Clear()
        txtLecturerContact.Clear()
        txtLecturerEmail.Clear()
        '
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles btnLoadLecturer.Click
        Try
            txtLecturerID.Text = lecturerDataGrid.SelectedRows(0).Cells(0).Value
            txtLecturerName.Text = lecturerDataGrid.SelectedRows(0).Cells(1).Value
            txtLecturerContact.Text = lecturerDataGrid.SelectedRows(0).Cells(2).Value
            txtLecturerEmail.Text = lecturerDataGrid.SelectedRows(0).Cells(3).Value
        Catch err As System.ArgumentOutOfRangeException
            MsgBox("You must select a row instead of a cell!", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnAddLecturer.Click
        If txtLecturerID.Text.Equals("") Then
            MsgBox("Lecturer ID cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtLecturerName.Text.Equals("") Then
            MsgBox("Lecturer Name cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtLecturerContact.Text.Equals("") Then
            MsgBox("Lecturer Contact cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtLecturerEmail.Text.Equals("") Then
            MsgBox("Lecturer Email cannot be left empty!", MsgBoxStyle.Critical)
        End If

        Dim lecturerID As Integer = CInt(txtLecturerID.Text)
        Dim lecturerName As String = CStr(txtLecturerName.Text)
        Dim lecturerContact As String = CStr(txtLecturerContact.Text)
        Dim lecturerEmail As String = CStr(txtLecturerEmail.Text)

        Dim foundLecturerID As Boolean = False
        For i As Integer = 0 To (lecturerDataGrid.Rows.Count - 1)
            If CInt(lecturerDataGrid.Rows(i).Cells(0).Value) = lecturerID Then
                foundLecturerID = True
            End If
        Next

        If foundLecturerID Then
            MsgBox("Lecturer ID already exist!", MsgBoxStyle.Critical)
        Else
            lecturerDataGrid.Rows.Add({lecturerID, lecturerName, lecturerContact, lecturerEmail})
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles btnUpdateLecturer.Click
        'Error Checking
        If txtLecturerID.Text.Equals("") Then
            MsgBox("Lecturer ID cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtLecturerName.Text.Equals("") Then
            MsgBox("Lecturer Name cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtLecturerContact.Text.Equals("") Then
            MsgBox("Lecturer Contact cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtLecturerEmail.Text.Equals("") Then
            MsgBox("Lecturer Email cannot be left empty!", MsgBoxStyle.Critical)
        End If

        Dim lecturerID As Integer
        Try
            lecturerID = CInt(txtLecturerID.Text)
        Catch err As System.InvalidCastException
            MsgBox("Lecturer ID cannot be a text value!", MsgBoxStyle.Critical)
        End Try

        Dim foundLecturerID As Boolean = False
        For i As Integer = 0 To (lecturerDataGrid.Rows.Count - 1)
            If CInt(lecturerDataGrid.Rows(i).Cells(0).Value) = lecturerID Then
                foundLecturerID = True
            End If
        Next

        If foundLecturerID Then
            lecturerDataGrid.Rows(lecturerID - 1).Cells(1).Value = txtLecturerName.Text
            lecturerDataGrid.Rows(lecturerID - 1).Cells(2).Value = txtLecturerContact.Text
            lecturerDataGrid.Rows(lecturerID - 1).Cells(3).Value = txtLecturerEmail.Text
        Else
            MsgBox("Lecturer ID does not exist!", MsgBoxStyle.Critical)
        End If
        'End of Error Checking'
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnDeleteLecturer.Click
        For Each row As DataGridViewRow In lecturerDataGrid.SelectedRows
            lecturerDataGrid.Rows.Remove(row)
        Next
    End Sub

    Private Sub btnManageSubjects_Click(sender As Object, e As EventArgs) Handles btnManageSubjects.Click
        subjectPanel.BringToFront()
    End Sub

    Private Sub btnManageLecturers_Click(sender As Object, e As EventArgs) Handles btnManageLecturers.Click
        lecturerPanel.BringToFront()
    End Sub

    Private Sub btnManageClasses_Click(sender As Object, e As EventArgs) Handles btnManageClasses.Click
        classesPanel.BringToFront()

        For Each row In subjectDataGrid.Rows
            cboSubjectName.Items.Add((row.Cells(1)).Value).ToString()
        Next

        For Each row In lecturerDataGrid.Rows
            cboLecturer.Items.Add((row.Cells(1)).Value).ToString()
        Next
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles btnClearAllClass.Click
        txtClassID.Clear()
        txtCreditHour.Clear()
        cboSubjectName.SelectedIndex = -1
        cboLecturer.SelectedIndex = -1
    End Sub

    Private Sub Button6_Click_1(sender As Object, e As EventArgs) Handles btnLoadClass.Click
        Try
            txtClassID.Text = classDataGrid.SelectedRows(0).Cells(0).Value
            cboSubjectName.SelectedValue = classDataGrid.SelectedRows(0).Cells(1).Value
            txtCreditHour.Text = classDataGrid.SelectedRows(0).Cells(2).Value
            cboSubjectName.SelectedValue = classDataGrid.SelectedRows(0).Cells(3).Value
        Catch err As System.ArgumentOutOfRangeException
            MsgBox("You must select a row instead of a cell!", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles btnManageStudentsCGPA.Click

    End Sub

    Private Sub classesPanel_Paint(sender As Object, e As PaintEventArgs) Handles classesPanel.Paint

    End Sub

    Private Sub cboSubjectName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSubjectName.SelectedIndexChanged

    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles btnAddClass.Click
        If txtClassID.Text.Equals("") Then
            MsgBox("Class ID cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf cboSubjectName.SelectedIndex = -1 Then
            MsgBox("Subject Name cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtCreditHour.Text.Equals("") Then
            MsgBox("Credit Hour cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf cboLecturer.SelectedIndex = -1 Then
            MsgBox("Lecturer Name cannot be left empty!", MsgBoxStyle.Critical)
        End If

        Dim classID As Integer = CInt(txtClassID.Text)
        Dim subjectName As String = CStr(cboSubjectName.SelectedItem.ToString())
        Dim creditHour As Integer = CInt(txtCreditHour.Text)
        Dim lecturerName As String = CStr(cboLecturer.SelectedItem.ToString())

        Dim foundClassID As Boolean = False
        For i As Integer = 0 To (classDataGrid.Rows.Count - 1)
            If CInt(classDataGrid.Rows(i).Cells(0).Value) = classID Then
                foundClassID = True
            End If
        Next

        If foundClassID Then
            MsgBox("Class ID already exist!", MsgBoxStyle.Critical)
        Else
            classDataGrid.Rows.Add({classID, subjectName, creditHour, lecturerName})
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles classDataGrid.CellContentClick

    End Sub

    Private Sub lecturerDataGrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles lecturerDataGrid.CellContentClick

    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles btnDeleteClass.Click
        For Each row As DataGridViewRow In classDataGrid.SelectedRows
            classDataGrid.Rows.Remove(row)
        Next
    End Sub

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles btnUpdateClass.Click
        'Error Checking
        If txtClassID.Text.Equals("") Then
            MsgBox("Class ID cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf cboSubjectName.SelectedIndex = -1 Then
            MsgBox("Subject Name cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf txtCreditHour.Text.Equals("") Then
            MsgBox("Credit Hour cannot be left empty!", MsgBoxStyle.Critical)
        ElseIf cboLecturer.SelectedIndex = -1 Then
            MsgBox("Lecturer Name cannot be left empty!", MsgBoxStyle.Critical)
        End If

        Dim classID As Integer
        Try
            classID = CInt(txtClassID.Text)
        Catch err As System.InvalidCastException
            MsgBox("Class ID cannot be a text value!", MsgBoxStyle.Critical)
        End Try

        Dim foundClassID As Boolean = False
        For i As Integer = 0 To (lecturerDataGrid.Rows.Count - 1)
            If CInt(classDataGrid.Rows(i).Cells(0).Value) = classID Then
                foundClassID = True
            End If
        Next

        If foundClassID Then
            classDataGrid.Rows(classID - 1).Cells(1).Value = cboSubjectName.SelectedItem.ToString
            classDataGrid.Rows(classID - 1).Cells(2).Value = txtCreditHour.Text
            classDataGrid.Rows(classID - 1).Cells(3).Value = cboLecturer.SelectedItem.ToString
        Else
            MsgBox("Class ID does not exist!", MsgBoxStyle.Critical)
        End If
        'End of Error Checking'
    End Sub

    Private Sub Student_Grading_System_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Login.Close()
    End Sub
End Class
